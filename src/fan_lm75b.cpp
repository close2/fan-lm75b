#define F_CPU 1000000

#include <stdint.h>
#include <avr/io.h>
#include <util/twi.h>
#include <util/delay.h>


#define BAUD 9600
#include <util/setbaud.h>

void uart_init(void) {
    UBRR0H = UBRRH_VALUE;
    UBRR0L = UBRRL_VALUE;

#if USE_2X
    UCSR0A |= _BV(U2X0);
#else
    UCSR0A &= ~(_BV(U2X0));
#endif

    UCSR0C = _BV(UCSZ01) | _BV(UCSZ00); /* 8-bit data */
    UCSR0B = _BV(RXEN0) | _BV(TXEN0);   /* Enable RX and TX */
}

void uart_putchar(char c) {
    loop_until_bit_is_set(UCSR0A, UDRE0); /* Wait until data register empty. */
    UDR0 = c;
}

void printHex4bit(uint8_t part) {
  if (part < 10) {
    uart_putchar('0' + part);
  } else {
    uart_putchar('A' + part - 10);
  }
}

void printTemp(int8_t temp) {
  uint8_t utempt = (uint8_t) temp;
  uint8_t part1 = (utempt & 0xF0) >> 4;
  uint8_t part2 = utempt & 0x0F;
  uart_putchar('T');
  uart_putchar(':');
  printHex4bit(part1);
  printHex4bit(part2);
  uart_putchar('\r');
  uart_putchar('\n');
}

// 1001 hardwired prefix of SMB75B. 000 selected. 1 read.
const uint8_t SLA_R = 0b10011111;

// At 10 the fan turns but is barely audible.
const uint8_t MIN_PWM = 10;
const int8_t LOW_TH = 55;
const int8_t HIGH_TH = 80;
const int8_t HYST = 3;

void waitForTWINT() {
  while (!(TWCR & _BV(TWINT)));
}

int8_t readTemperatur() {
  // Send start condition:
  TWCR = _BV(TWINT) | _BV(TWSTA) | _BV(TWEN);
  
  // Wait for TWINT Flag set.
  // This indicates that the START condition has been transmitted.
  waitForTWINT();
  
  // Check value of TWI Status Register. Mask prescaler bits.
  // If status different from START return error.
  if ((TWSR & 0xF8) != TW_START) return -128;
  
  // Load SLA_R into TWDR Register.
  // Clear TWINT bit in TWCR to start transmission of address. 
  TWDR = SLA_R;
  TWCR = _BV(TWINT) | _BV(TWEN);
  
  // Wait for TWINT Flag set.
  // This indicates that the SLA+R has been transmitted,
  // and ACK/NACK has been received. 
  waitForTWINT();
  
  // Check value of TWI Status Register. Mask prescaler bits.
  // If status different from MT_SLA_ACK return error.
  if ((TWSR & 0xF8) != TW_MR_SLA_ACK) return -127;
  
  TWCR = _BV(TWINT) | _BV(TWEA) | _BV(TWEN);
  waitForTWINT();
  if ((TWSR & 0xF8) != TW_MR_DATA_ACK) return -126;
  
  uint8_t temperatur = TWDR;
  
  // ignore the second byte and send nack
  TWCR = _BV(TWINT) | _BV(TWEA) | _BV(TWEN);
  waitForTWINT();
  uint8_t ignore = TWDR;  // I don't think that reading the byte is necessary, but it won't hurt.
  (void) ignore; // Tell compiler to ignore this variable.
  
  // send nack
  TWCR = _BV(TWINT) | _BV(TWEN);
  waitForTWINT();
  
  if ((TWSR & 0xF8) != TW_MR_DATA_NACK) return -125;
  
  TWCR = _BV(TWINT) | _BV(TWSTO) | _BV(TWEN);
  
  return temperatur;
}

void initialize() {
  // Set OC0A to output (PD6)
  DDRD = _BV(6);
  // PWM
  TCCR0A |= _BV(COM0A1); // Non inverting mode.
  TCCR0A |= _BV(WGM01) | _BV(WGM00); // Fast PWM.
  
  // No prescaler.
  TCCR0B |= _BV(CS00);
  
  
  // Nothing to do for I²C (LM75B) initialization.
  // SDA == PC4
  // SCL == PC5
  readTemperatur();
}

enum class Mode {
  OFF,
  MEDIUM,
  HIGH
};

__attribute__ ((OS_main)) int main(void) {
  uart_init();
  initialize();
  
  
  auto mode = Mode::HIGH;
  
  uint8_t currentPwm = 0xFF;
  OCR0A = currentPwm;
  
  uint8_t mediumPwm = 30;
  
  uint8_t incorrectReadings = 0;
  
  for(;;) {
    _delay_ms(500);
    auto temperatur = readTemperatur();
    
    printTemp(temperatur);
    if (temperatur < -100) {
      if (incorrectReadings < 10) {
        incorrectReadings++;
      } else {
        // Turn on full speed.  We don't know the temperatur.
        OCR0A = 0xFF;
      }
      continue;
    }
    
    incorrectReadings = 0;
    
    switch(mode) {
      case Mode::OFF:
        if (temperatur > (LOW_TH + HYST)) {
          mode = Mode::MEDIUM;
          currentPwm = mediumPwm;
        }
        break;
      case Mode::HIGH:
        if (temperatur < (HIGH_TH - HYST)) {
          mode = Mode::MEDIUM;
          currentPwm = mediumPwm;
        }
        break;
      case Mode::MEDIUM:
        if (temperatur > (HIGH_TH + HYST)) {
          mode = Mode::HIGH;
          if (mediumPwm < (0xFF - 10)) mediumPwm++;
          currentPwm = 0xFF;
        } else if (temperatur < (LOW_TH - HYST)) {
          mode = Mode::OFF;
          if (mediumPwm > MIN_PWM + 10) mediumPwm--;
          currentPwm = MIN_PWM;
        }
        break;
    }
    
    OCR0A = currentPwm;
  }
  return 0;
}
